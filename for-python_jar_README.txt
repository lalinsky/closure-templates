// Copyright 2014 Google Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


Contents:

+ SoyToPySrcCompiler.jar
    Executable jar that compiles template files into Python files.

+ soy/
    Python package with helper utilities required by all Python code
    that SoyToPySrcCompiler generates.


Instructions:

+ A simple Hello World for Python:
    https://bitbucket.org/lalinsky/closure-templates/wiki/Hello%20World%20Using%20Python

+ Complete documentation:
    http://code.google.com/closure/templates/

+ Closure Templates project on Google Code:
    http://code.google.com/p/closure-templates/

+ Python Closure Templates project on Bitbucket:
    https://bitbucket.org/lalinsky/closure-templates


Notes:

+ Closure Templates requires Java 6 or higher:
    http://www.java.com/
