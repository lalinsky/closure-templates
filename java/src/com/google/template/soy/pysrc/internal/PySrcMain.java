/*
 * Copyright 2008 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.template.soy.pysrc.internal;

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.io.Files;
import com.google.inject.Inject;
import com.google.inject.Key;
import com.google.inject.Provider;
import com.google.template.soy.base.BaseUtils;
import com.google.template.soy.base.SoySyntaxException;
import com.google.template.soy.internal.i18n.BidiGlobalDir;
import com.google.template.soy.internal.i18n.SoyBidiUtils;
import com.google.template.soy.pysrc.SoyPySrcOptions;
import com.google.template.soy.msgs.SoyMsgBundle;
import com.google.template.soy.msgs.internal.InsertMsgsVisitor;
import com.google.template.soy.msgs.internal.InsertMsgsVisitor.EncounteredPlrselMsgException;
import com.google.template.soy.shared.internal.ApiCallScopeUtils;
import com.google.template.soy.shared.internal.GuiceSimpleScope;
import com.google.template.soy.shared.restricted.ApiCallScopeBindingAnnotations.ApiCall;
import com.google.template.soy.shared.restricted.ApiCallScopeBindingAnnotations.IsUsingIjData;
import com.google.template.soy.shared.restricted.ApiCallScopeBindingAnnotations.IsUsingDelPackages;
import com.google.template.soy.sharedpasses.IsUsingDelPackagesVisitor;
import com.google.template.soy.sharedpasses.IsUsingIjDataVisitor;
import com.google.template.soy.sharedpasses.opti.SimplifyVisitor;
import com.google.template.soy.soytree.SoyFileNode;
import com.google.template.soy.soytree.SoyFileSetNode;
import com.google.template.soy.soytree.SoySyntaxExceptionUtils;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.List;

import javax.annotation.Nullable;


/**
 * Main entry point for the Python Src backend (output target).
 *
 * <p> Important: Do not use outside of Soy code (treat as superpackage-private).
 *
 * @author Kai Huang
 */
public class PySrcMain {


  /** The scope object that manages the API call scope. */
  private final GuiceSimpleScope apiCallScope;

  /** The instanceof of SimplifyVisitor to use. */
  private final SimplifyVisitor simplifyVisitor;

  /** Provider for getting an instance of OptimizeBidiCodeGenVisitor. */
  private final Provider<OptimizeBidiCodeGenVisitor> optimizeBidiCodeGenVisitorProvider;

  /** Provider for getting an instance of GenPyCodeVisitor. */
  private final Provider<GenPyCodeVisitor> genPyCodeVisitorProvider;


  /**
   * @param apiCallScope The scope object that manages the API call scope.
   * @param simplifyVisitor The instance of SimplifyVisitor to use.
   * @param optimizeBidiCodeGenVisitorProvider Provider for getting an instance of
   *     OptimizeBidiCodeGenVisitor.
   * @param genPyCodeVisitorProvider Provider for getting an instance of GenPyCodeVisitor.
   */
  @Inject
  public PySrcMain(
      @ApiCall GuiceSimpleScope apiCallScope, SimplifyVisitor simplifyVisitor,
      Provider<OptimizeBidiCodeGenVisitor> optimizeBidiCodeGenVisitorProvider,
      Provider<GenPyCodeVisitor> genPyCodeVisitorProvider) {
    this.apiCallScope = apiCallScope;
    this.simplifyVisitor = simplifyVisitor;
    this.optimizeBidiCodeGenVisitorProvider = optimizeBidiCodeGenVisitorProvider;
    this.genPyCodeVisitorProvider = genPyCodeVisitorProvider;
  }


  /**
   * Generates Python source code given a Soy parse tree, an options object, and an optional bundle of
   * translated messages.
   *
   * @param soyTree The Soy parse tree to generate Python source code for.
   * @param pySrcOptions The compilation options relevant to this backend.
   * @param msgBundle The bundle of translated messages, or null to use the messages from the Soy
   *     source.
   * @return A list of strings where each string represents the Python source code that belongs in one
   *     Python file. The generated Python files correspond one-to-one to the original Soy source files.
   * @throws SoySyntaxException If a syntax error is found.
   */
  public List<String> genPySrc(
      SoyFileSetNode soyTree, SoyPySrcOptions pySrcOptions, @Nullable SoyMsgBundle msgBundle)
      throws SoySyntaxException {

    // Generate code with the ijData param if either (a) the user specified the compiler flag
    // --isUsingIjData or (b) any of the Soy code in the file set references injected data.
    boolean isUsingIjData =
        pySrcOptions.isUsingIjData() || (new IsUsingIjDataVisitor()).exec(soyTree);

    // Generate code with the delPackages param if either (a) the user specified the compiler flag
    // --isUsingDelPackages or (b) any of the Soy code in the file set defines any delegate package.
    boolean isUsingDelPackages =
        pySrcOptions.isUsingDelPackages() || (new IsUsingDelPackagesVisitor().exec(soyTree));

    try {
      (new InsertMsgsVisitor(msgBundle, false)).exec(soyTree);
    } catch (EncounteredPlrselMsgException e) {
      throw SoySyntaxExceptionUtils.createWithNode(
          "PySrc backend doesn't support plural/select messages.", e.msgNode);
    }

    apiCallScope.enter();
    try {
      // Seed the scoped parameters.
      apiCallScope.seed(SoyPySrcOptions.class, pySrcOptions);
      apiCallScope.seed(Key.get(Boolean.class, IsUsingIjData.class), isUsingIjData);
      apiCallScope.seed(Key.get(Boolean.class, IsUsingDelPackages.class), isUsingDelPackages);
      BidiGlobalDir bidiGlobalDir = SoyBidiUtils.decodeBidiGlobalDir(
          pySrcOptions.getBidiGlobalDir());
      ApiCallScopeUtils.seedSharedParams(apiCallScope, msgBundle, bidiGlobalDir);

      // Do the code generation.
      optimizeBidiCodeGenVisitorProvider.get().exec(soyTree);
      simplifyVisitor.exec(soyTree);
      return genPyCodeVisitorProvider.get().exec(soyTree);

    } finally {
      apiCallScope.exit();
    }
  }


  /**
   * Generates Python source files given a Soy parse tree, an options object, an optional bundle of
   * translated messages, and information on where to put the output files.
   *
   * @param soyTree The Soy parse tree to generate Python source code for.
   * @param pySrcOptions The compilation options relevant to this backend.
   * @param msgBundle The bundle of translated messages, or null to use the messages from the Soy
   *     source.
   * @param outputPathFormat The format string defining how to build the output file path
   *     corresponding to an input file path.
   * @param inputPathsPrefix The input path prefix, or empty string if none.
   * @throws SoySyntaxException If a syntax error is found.
   * @throws IOException If there is an error in opening/writing an output Python file.
   */
  public void genPyFiles(
      SoyFileSetNode soyTree, SoyPySrcOptions pySrcOptions,
      @Nullable SoyMsgBundle msgBundle, String outputPathFormat, String inputPathsPrefix)
      throws SoySyntaxException, IOException {

    List<String> pyFileContents = genPySrc(soyTree, pySrcOptions, msgBundle);

    int numFiles = soyTree.numChildren();
    if (numFiles != pyFileContents.size()) {
      throw new AssertionError();
    }

    // Maps output paths to indices of inputs that should be emitted to them.
    Multimap<String, Integer> outputs = Multimaps.newListMultimap(
        Maps.<String, Collection<Integer>>newLinkedHashMap(),
        new Supplier<List<Integer>>() {
          @Override
          public List<Integer> get() {
            return Lists.newArrayList();
          }
        });

    // First, check that the parent directories for all output files exist, and group the output
    // files by the inputs that go there.
    // This means that the compiled source from multiple input files might be written to a single
    // output file, as is the case when there are multiple inputs, and the output format string
    // contains no wildcards.
    for (int i = 0; i < numFiles; ++i) {
      SoyFileNode inputFile = soyTree.getChild(i);
      String inputFilePath = inputFile.getFilePath();
      String outputFilePath =
          PySrcUtils.buildFilePath(outputPathFormat, inputFilePath, inputPathsPrefix);

      BaseUtils.ensureDirsExistInPath(outputFilePath);
      outputs.put(outputFilePath, i);
    }

    for (String outputFilePath : outputs.keySet()) {
      Writer out = Files.newWriter(new File(outputFilePath), Charsets.UTF_8);
      try {
        for (int inputFileIndex : outputs.get(outputFilePath)) {
          out.write(pyFileContents.get(inputFileIndex));
        }
      } finally {
        out.close();
      }
    }
  }

}
